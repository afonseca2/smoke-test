import json
import requests
import os

project_id = os.environ['PROJECT_ID']
token = os.environ['API_TOKEN']
domain = os.environ['URL']


headers = {
    'Private-Token': token
}

project_url = 'https://'+domain+'/api/v4/projects/'+project_id


# DELETE /projects/:id/variables/:key
url_var =  project_url +'/variables/'+'MR_ID'
response = requests.delete(url_var, headers=headers, verify=False)

url_var =  project_url +'/variables/'+'ISSUE_ID'
response = requests.delete(url_var, headers=headers, verify=False)