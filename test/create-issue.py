import json
import requests
import os

project_id = os.environ['PROJECT_ID']
token = os.environ['API_TOKEN']
domain = os.environ['URL']


headers = {
    'Private-Token': token
}

data = {
"title" : "Smoke test Issue",
   "state" : "opened",
   "upvotes": 4,
   "downvotes": 0,
   "merge_requests_count": 0,
   "description" : "This is a smoke test issue...",
}

# POST /projects/:id/issues
project_url =  'https://'+domain+'/api/v4/projects/'+project_id
issue_url = project_url+'/issues'

response = requests.post(issue_url, headers=headers, data=data, verify=False)
response_dict = json.loads(response.text)
for i in response_dict:
    print("key: ", i, "val: ", response_dict[i])
issue_id = response_dict['iid']

data = {
    "key": "ISSUE_ID",
    "value": issue_id,
    "variable_type": "env_var",
    "protected": "false",
    "masked": "false",
    "environment_scope": "*"
}

url_var =  project_url +'/variables'
response = requests.post(url_var, headers=headers, data=data, verify=False)