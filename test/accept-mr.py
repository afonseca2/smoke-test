import json
import requests
import os

project_id = os.environ['PROJECT_ID']
token = os.environ['API_TOKEN']
merge_request_iid = os.environ['MR_ID']
domain = os.environ['URL']



headers = {
    'Private-Token': token
}

url =  'https://'+domain+'/api/v4/projects/'+project_id+'/merge_requests/'+merge_request_iid+'/merge'
response = requests.put(url, headers=headers, verify=False)
response_dict = json.loads(response.text)
for i in response_dict:
    print("key: ", i, "val: ", response_dict[i])