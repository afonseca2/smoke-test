import json
import requests
import os

project_id = os.environ['PROJECT_ID']
token = os.environ['API_TOKEN']
domain = os.environ['URL']

headers = {
    'Private-Token': token
}

project_url =  'https://'+domain+'/api/v4/projects/'+project_id
url_var =  project_url +'/variables/PIPELINE_LOCKED'

response = requests.delete(url_var, headers=headers, verify=False)