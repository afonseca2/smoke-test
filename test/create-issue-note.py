import json
import requests
import os

project_id = os.environ['PROJECT_ID']
token = os.environ['API_TOKEN']
issue_id = os.environ['ISSUE_ID']
domain = os.environ['URL']
user = os.environ['USER']

headers = {
    'Private-Token': token
}

data = {
"title" : "Smoke test Issue",
   "state" : "opened",
   "upvotes": 4,
   "downvotes": 0,
   "merge_requests_count": 0,
   "description" : "This is a smoke test issue...",
}

# POST /projects/:id/issues
project_url =  'https://gitlab.com/api/v4/projects/'+project_id
note = 'The supreme administrator @'+user+' has left this issue comment'
issue_note_url = project_url+'/issues/'+issue_id+'/notes?body='+note
response = requests.post(issue_note_url, headers=headers, verify=False)