#def boolean isCmsReady(String env){
#  ready = null
#  while(ready != '200'){
#    ready = sh(returnStdout: true, script: "curl -o -I -L -s -w %{http_code} " + env + " || true").trim()
#  }
#  return true
#}
import requests
import json
import os

domain = os.environ['PAGES_URL']

url =  'https://'+domain+'/smoke-test/'
response = requests.get(url, verify=False)

if response.status_code == 200:
    print ('Pages Running.')
else:
    raise ValueError('GitLab Pages Error detected.')