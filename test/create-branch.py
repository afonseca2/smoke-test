import json
import requests
import os

project_id = os.environ['PROJECT_ID']
token = os.environ['API_TOKEN']
domain = os.environ['URL']


headers = {
    'Private-Token': token
}

url =  'https://'+domain+'/api/v4/projects/'+project_id+'/repository/branches?branch=smoke-branch&ref=master'

response = requests.post(url, headers=headers, verify=False)
response_dict = json.loads(response.text)
for i in response_dict:
    print("key: ", i, "val: ", response_dict[i])