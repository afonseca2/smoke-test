import json
import requests
import os

project_id = os.environ['PROJECT_ID']
token = os.environ['API_TOKEN']
issue_id = os.environ['ISSUE_ID']
domain = os.environ['URL']


headers = {
    'Private-Token': token
}

project_url = 'https://'+domain+'/api/v4/projects/'+project_id
mr_url =  project_url+'/merge_requests'

data = {
  "project_id": project_id,
  "title": "smoke-test MR",
  "description": "Closes #"+issue_id,
  "target_branch": "master",
  "source_branch": "smoke-branch",
  "work_in_progress": "true",
  "remove_source_branch" : "true"
}

response = requests.post(mr_url, headers=headers, data=data, verify=False)
response_dict = json.loads(response.text)
for i in response_dict:
    print("key: ", i, "val: ", response_dict[i])
mr_id = response_dict['iid']

data = {
    "key": "MR_ID",
    "value": mr_id,
    "variable_type": "env_var",
    "protected": "false",
    "masked": "false",
    "environment_scope": "*"
}

url_var =  project_url +'/variables'
response = requests.post(url_var, headers=headers, data=data, verify=False)