import json
import requests
import os
import random

project_id = os.environ['PROJECT_ID']
token = os.environ['API_TOKEN']
domain = os.environ['URL']

headers = {
    'Private-Token': token
}
random_commit = random.getrandbits(128)
file_name = 'Lockfile'

url =  'https://'+domain+'/api/v4/projects/'+project_id+'/repository/files/'+file_name
branch = 'smoke-branch'

data = {
    "branch": branch, 
    "author_email": "author@example.com",
    "author_name": "Firstname Lastname",
    "content": "Lockfile: "+str(random_commit),
    "commit_message": "create a new file"
}

response = requests.post(url, headers=headers, data=data, verify=False)

project_url =  'https://'+domain+'/api/v4/projects/'+project_id

data = {
    "key": "FILE_COMMITED",
    "value": file_name,
    "variable_type": "env_var",
    "protected": "false",
    "masked": "false",
    "environment_scope": "*"
}

url_var =  project_url +'/variables'
response = requests.post(url_var, headers=headers, data=data, verify=False)
