
# GitLab Smoke Test

This project is meant to be a smoke-test suite to validate a GitLab Deployment through API interaction.
Currently, is automated way to interact with the API, creating some objects that will be minimally validating the architecture, base in the assumption that this tasks couldn't be accomplish if any GitLab component is not running.

Additionaly to this test we recommend Professional Service Engineers run the following checklist:

[Customer Completation Checklist](https://docs.google.com/document/d/1iTns1MhB0y6E9AdX5nmCA9fmK5ATdjmxuOxvMoH_4Qw) (Internal Only)


# What this project does?

- Deploy pages;
- Create an Issue;
- Make comment in a issue mentioning @root user;
- create a branch;
- commit a file in the branch previously created;
- creates a Merge Request mentioning the issue;
- Merges the Previously created MR;

Obs: It is required to have pages enabled.

# How to use

## Manual fashion

Once you have this project imported in your intance you need to create the following CI/CD variables:

 - URL - Your GitLab URL;
 - PAGES_URL - the url where pages is being exposed;
 - API_TOKEN - Token with API scope enabled;
 - PROJECT_ID - this is the id from this project in your GitLab Server.
 - USER - User that will be mentioned in the issue comment;
 - PIPELINE_LOCKED - Must have true as value to be able to run the pipeline (this variable will be removed)

 With the above variables in place you need to execute the pipeline manually.

## Automation fashion

It is possible to use a python script (import-deploy-project.py ) to import a project and trigger the pipeline execution, from this point on, everything will be done through the Pipeline. Of course this can be done using an automation tool (like puppet, ansible, etc). Following we have the steps required for doing so:

- Export this project in in tar.gz format. This can be done exporting this project from gitlab at Project -> Settings -> General -> Advanced - Export. 
- Place the exported project file in the `import` folder.
- Set the following environment variables:
 - URL
 - PAGES_URL
 - API_TOKEN
 - USER - User that will be mentioned in the issue comment; 

- Finally you need to run the import script script using python3 as follows:

```
python3 import-deploy-project.py 
```
This will import the project and triggers its pipeline execution firing all the process.

Obs: the import-deploy-project.py relies that the variables above will be available as environment variables in the host or container where the file is being executed.

## TODOS

- Creates an additional stage to query for the created objects failing the pipeline if any object is not returned when requested;
- GitLab Pages smoke test;
  - In the future, it will be possible to test the page's deployment. Currently, this is not possible to automate because the API does not expose methods to enable, and change pages access level through yet;
- Create push using ssh authentication;
- Create smoke-test to package registry;
- Create smoke-test to docker registry; 

 # Known Issue

Pipeline as marked as invalid when PIPELINE_LOCKED is not defined or false due depencied that cleanup has with the merge stage that won't be rendered.