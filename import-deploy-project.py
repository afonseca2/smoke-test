import json
import requests
import time
import os
from io import BytesIO

f = open('import/pages_export.tar.gz', 'rb')

domain = os.environ['URL']
domain_pages = os.environ['PAGES_URL']
api_token = os.environ['API_TOKEN']
user = os.environ['USER']

url =  'https://'+domain+'/api/v4/projects/import'

files = {
    'file': ('pages-master.tar.gz', f)
}

data = {
    "path": "smoke-test",
}

headers = {
    'Private-Token': api_token
}

response = requests.post(url, headers=headers, data=data, files=files, verify=False)
response_dict = json.loads(response.text)
for i in response_dict:
    print("key: ", i, "val: ", response_dict[i])

project_id = response_dict['id']
project_url = 'https://'+domain+'/api/v4/projects/'+str(project_id)

data = {
    "key": "PROJECT_ID",
    "value": str(project_id),
    "variable_type": "env_var",
    "protected": "false",
    "masked": "false",
    "environment_scope": "*"
}

url_var =  project_url +'/variables'
response = requests.post(url_var, headers=headers, data=data, verify=False)

data = {
    "key": "API_TOKEN",
    "value": api_token,
    "variable_type": "env_var",
    "protected": "false",
    "masked": "false",
    "environment_scope": "*"
}
response = requests.post(url_var, headers=headers, data=data, verify=False)

data = {
    "key": "URL",
    "value": domain,
    "variable_type": "env_var",
    "protected": "false",
    "masked": "false",
    "environment_scope": "*"
}
response = requests.post(url_var, headers=headers, data=data, verify=False)

data = {
    "key": "PAGES_URL",
    "value": domain_pages,
    "variable_type": "env_var",
    "protected": "false",
    "masked": "false",
    "environment_scope": "*"
}
response = requests.post(url_var, headers=headers, data=data, verify=False)

data = {
    "key": "USER",
    "value": user,
    "variable_type": "env_var",
    "protected": "false",
    "masked": "false",
    "environment_scope": "*"
}
response = requests.post(url_var, headers=headers, data=data, verify=False)



branch = 'master'
url_pipeline =  project_url +'/pipeline?ref='+branch
response = requests.post(url_pipeline, headers=headers, verify=False)